FROM ubuntu:focal
RUN apt-get update && apt-get upgrade -y 
RUN apt-get install sudo wget curl xauth -y 
RUN apt-get update && apt-get install -y alsa-base alsa-utils apulse libpulse0 libasound2 pulseaudio pulseaudio-utils -y
RUN apt install gnupg2 -y
RUN curl -sS https://download.spotify.com/debian/pubkey_5E3C45D7B312C643.gpg | apt-key add - 
RUN echo "deb http://repository.spotify.com stable non-free" | tee /etc/apt/sources.list.d/spotify.list
RUN sudo apt-get update && sudo apt-get install spotify-client -y