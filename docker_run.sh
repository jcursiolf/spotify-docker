IMAGE="spotify-app"
CONTAINER="teste_spotify"
USER_UID=$(id -u)
USER_NAME=$USER
echo ${USER_UID}
echo ${USER_NAME}

docker run -it \
--net=host \
--device=/dev/video0:/dev/video0 \
--volume=/tmp/.X11-unix \
-e DISPLAY \
-e "PULSE_SERVER=unix:/home/socket" \
-e QT_GRAPHICSSYSTEM=native \
--device /dev/snd \
--volume=/etc/machine-id:/etc/machine-id \
--volume=/var/lib/dbus:/var/lib/dbus \
--volume=/run/user/${USER_UID}/pulse/native:/home/socket \
--volume=/home/${USER_NAME}/.config/pulse/cookie:/root/.config/pulse/cookie \
--privileged \
--name ${CONTAINER} ${IMAGE} bash


# In the host, run:
# xauth list and copy the line that looks like this:
# CPQGES7XV6LA/unix:  MIT-MAGIC-COOKIE-1  6b7de55030b0c27b39fe6dc6462ccc59
# It may be necessary to add 0 or 1 after "/unix:".
# Inside docker, run
# $ xauth add CPQGES7XV6LA/unix:1  MIT-MAGIC-COOKIE-1 6b7de55030b0c27b39fe6dc6462ccc59
# $ export "PULSE_SERVER=unix:/home/socket" 