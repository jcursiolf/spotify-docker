# README

This is a docker container running Spotify in a Ubuntu 20.04 system.

## Steps

1. To create the docker image, run `docker_build.sh`.
2. To create the docker container, run `docker_run.sh`.

## Enabling X11

In the host, run:

```
$ xauth list
<hostname>/unix:  MIT-MAGIC-COOKIE-1  <hexadecimal_hash>
#ffff#435051474553375856364c41#:  MIT-MAGIC-COOKIE-1  <hexadecimal_hash>
```

Copy the first line of the  output. It may be necessary to add 0 or 1 after "/unix:".
Inside the docker, run:

```
$ xauth add <hostname>/unix:1  MIT-MAGIC-COOKIE-1 <hexadecimal_hash>
```